# Vuejs input with list, for hidden values

> Set different key for the value and the display

### Fuzzy searcher!
> You'll need to install fuzzy-search from npm.
```
$ npm i -s fuzzy-search
```

### Use the input

```
 # import and register the component as you would normally do
 <listselect
  style="width:300px;" <- otherwise it will go from end to end
  inputType="number" <- text by default
  inputClass="custom-select-input-class"
  inputPlaceholder="search something"
  :list="MyArrayOfObjects"
  displayKey="name"
  valueKey="cost"
  v-on:listValue="MyLocalVariable = $event" <- set the MyLocalVariable var to what the user selected
 ></listselect>


```
